const Poi = require("../models/poi.model.js");

// Create and Save a new POI
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a POI
  const poi = new Poi({
    idPOI: req.body.idPOI,
    poi_name: req.body.poi_name,
    poi_email: req.body.poi_email,
    poi_geo: req.body.poi_geo,
    poi_hidden: req.body.poi_hidden,
  });

  // Save POI in the database
  Poi.create(poi, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the POI."
      });
    else res.send(data);
  });
};

// Retrieve all Pois from the database.
exports.findAll = (req, res) => {
  Poi.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Pois."
      });
    else res.send(data);
  });
};

// Find a single Poi with a idPOI
exports.findOne = (req, res) => {
  Poi.findById(req.params.idPOI, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Poi with id ${req.params.idPOI}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Poi with id " + req.params.idPOI
        });
      }
    } else res.send(data);
  });
};

// Update a Poi identified by the idPOI in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  console.log(req.body);

  Poi.updateById(
    req.params.idPOI,
    new Poi(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Poi with id ${req.params.idPOI}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Poi with id " + req.params.idPOI
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a Poi with the specified idPOI in the request
exports.delete = (req, res) => {
  Poi.remove(req.params.idPOI, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Poi with id ${req.params.idPOI}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Poi with id " + req.params.idPOI
        });
      }
    } else res.send({ message: `Poi was deleted successfully!` });
  });
};

// Delete all Pois from the database.
exports.deleteAll = (req, res) => {
  Poi.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Pois."
      });
    else res.send({ message: `All Pois were deleted successfully!` });
  });
};
