module.exports = app => {
  const Poi = require("../controllers/poi.controller.js");

  // Create a new poi
  app.post("/pois/addpoi", Poi.create);

  // Retrieve all pois
  app.get("/pois", Poi.findAll);

  // Retrieve a single poi with idPOI
  app.get("/pois/:idPOI", Poi.findOne);

  // Update a poi with idPOI
  app.put("/pois/update/:idPOI", Poi.update);

  // Delete a poi with idPOI
  app.delete("/pois/delete/:idPOI", Poi.delete);

  // Delete a all poi
  app.delete("/pois", Poi.deleteAll);


};
